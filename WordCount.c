#include<stdio.h>
#include<string.h>
int main(int argc,char *argv[])
{
    FILE *fp;
    int cnt=0;
    char words[1024];
    fp = fopen(argv[2],"r");
    if(strcmp(argv[1],"-c")==0||strcmp(argv[1],"-C")==0)
    {
        //每次读取一个字节，直到读取完毕
         while( fgetc(fp) != EOF ){
                cnt++;
         }
         fclose(fp);
         printf("字符数为:%d\n",cnt);
    }
    if(strcmp(argv[1],"-w")==0||strcmp(argv[1],"-W")==0)
    {
        //用fgets（）循环读取新行
        while(fgets(words,1024,fp))
        {
            int ispace=1;//存储字符的状态:是否为空格或逗号
           int length=strlen(words);
           for(int i=0;i<length;i++)
           {//如果当前字符为空格 且上一个字符不是空格则cnt+1
               if(words[i]==' '||words[i]==','||words[i]=='\n')
               {
                   if(ispace==0){
                       cnt++;
                       ispace=1;
                   }
               }
              else  {
                        ispace=0; }//如果不是空格置为0

           }
           if(ispace==0){//加上每一行的最后一个单词（如果有的话）
               cnt++;
           }
        }
         printf("单词数为:%d\n",cnt);
    }
}